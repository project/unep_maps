<?php

namespace Drupal\unep_maps\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use geoPHP;

/**
 * The UNEP MAPS data service.
 */
class UnepMapsDataService {

  use MessengerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * The module handler to invoke hooks.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected ModuleHandler $moduleHandler;

  /**
   * The UnepMapsDataService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Renderer $renderer, ModuleHandler $moduleHandler) {
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Get data objects for rendering markers on mapbox.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param string $dataSource
   *   The data source field id.
   * @param string $popupSource
   *   The popup source field id.
   *
   * @return array
   *   The formatted pin data.
   */
  public function getPinData(ViewExecutable $view, string $dataSource, string $popupSource) {
    $rows = $view->result;
    $data = [];
    foreach ($rows as $row) {
      $entity = $this->getEntity($row, $dataSource);
      $coordinates = $entity->get($dataSource)->getValue();
      $coordinates = reset($coordinates);
      if (empty($coordinates) || $coordinates['geo_type'] !== 'Point') {
        continue;
      }
      // Mapbox uses lng - lat format.
      $pinCoordinates = [$coordinates['lon'], $coordinates['lat']];
      $data[] = [
        'coordinates' => $pinCoordinates,
        'popup' => $this->getPopupContent($view, $row, $popupSource, 'pin'),
      ];
    }

    return $data;
  }

  /**
   * Get data objects for highlighting country polygons on mapbox.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param string $dataSource
   *   The data source field id.
   * @param string $popupSource
   *   The popup source field id.
   * @param string $linkSource
   *   The link source field id.
   *
   * @return array
   *   The formatted country data.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCountryData(ViewExecutable $view, string $dataSource, string $popupSource, string $linkSource) {
    $rows = $view->result;
    $data = [];
    foreach ($rows as $row) {
      $entity = $this->getEntity($row, $dataSource);
      $iso3 = $entity->get($dataSource)->value;
      if (empty($iso3)) {
        continue;
      }
      $data[] = [
        'iso3' => $entity->get($dataSource)->value,
        'popup' => $this->getPopupContent($view, $row, $popupSource, 'country'),
        'link' => $this->getRedirectLink($view, $row, $linkSource),
      ];
    }

    \Drupal::moduleHandler()->alter('unep_maps_country_data', $data);

    return $data;
  }

  /**
   * Get data objects for rendering area polygons on mapbox.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param string $dataSource
   *   The data source field id.
   * @param string $popupSource
   *   The popup source field id.
   *
   * @return array
   *   The formatted area data.
   *
   * @SuppressWarnings(PHPMD.StaticAccess)
   */
  public function getAreaData(ViewExecutable $view, string $dataSource, string $popupSource) {
    $rows = $view->result;
    $data = [
      'type' => 'FeatureCollection',
      'features' => [],
    ];
    foreach ($rows as $row) {
      $entity = $this->getEntity($row, $dataSource);
      $coordinates = $entity->get($dataSource)->getValue();
      $coordinates = reset($coordinates);
      if (empty($coordinates) || $coordinates['geo_type'] !== 'Polygon') {
        continue;
      }
      $wktPolygon = $entity->get($dataSource)->value;
      try {
        $geometry = GeoPHP::load($wktPolygon, 'wkt');
        // Create a GeoJSON feature.
        $geoJsonGeometry = json_decode($geometry->out('json'), TRUE);

        // Create the GeoJSON feature structure.
        $geoJsonFeature = [
          'type' => 'Feature',
          'geometry' => $geoJsonGeometry,
          'properties' => [
            'id' => $entity->id(),
            'popup' => $this->getPopupContent($view, $row, $popupSource, 'area'),
          ],
        ];

        $data['features'][] = $geoJsonFeature;
      }
      catch (\Exception) {
        continue;
      }
    }

    return $data;
  }

  /**
   * Gets url for GeoJson boundaries for clear map.
   *
   * @return string|null
   *   The absolute url for the GeoJson.
   */
  public function getClearMapSource() {
    $modulePath = $this->moduleHandler->getModule('unep_maps')
      ->getPath();
    $url = Url::fromUserInput('/' . $modulePath . '/assets/country_boundaries/country_polygon.json');
    return $url->setAbsolute()->toString();
  }

  /**
   * Renders popup content after calling altering hooks.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param \Drupal\views\ResultRow $row
   *   The current row.
   * @param string $popupSource
   *   The field id of the popup source.
   * @param string $renderItem
   *   The name of the render item. One of pin, area or country,.
   *
   * @return callable|\Drupal\Component\Render\MarkupInterface|mixed|void|null
   *   The rendered item or null.
   */
  protected function getPopupContent(ViewExecutable $view, ResultRow $row, string $popupSource, string $renderItem) {
    if (empty($popupSource) || $popupSource == '_none') {
      return NULL;
    }
    $renderValue = $view->field[$popupSource]->render($row);
    if (!is_array($renderValue)) {
      return $this->renderField($view, $row, $popupSource);
    }

    $this->moduleHandler->invokeAll('unep_maps_' . $renderItem . '_tooltip_data_alter', [&$renderValue]);
    return $this->renderer->renderPlain($renderValue);
  }

  /**
   * Gets entity with the field id form view or relationships.
   *
   * @param \Drupal\views\ResultRow $row
   *   The view's row.
   * @param string $fieldId
   *   The field id.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   */
  protected function getEntity(ResultRow $row, string $fieldId) {
    $entity = $row->_entity;
    $relationShipEntities = $row->_relationship_entities;
    if ($entity->hasField($fieldId)) {
      return $entity;
    }

    foreach ($relationShipEntities as $relationShipEntity) {
      if ($relationShipEntity->hasField($fieldId)) {
        return $relationShipEntity;
      }
    }

    return $entity;
  }

  /**
   * Gets link to redirect to when clicking on a territory.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param \Drupal\views\ResultRow $row
   *   The current row.
   * @param string $linkSource
   *   The link's source.
   *
   * @return string|null
   *   The url if it exists, otherwise null.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRedirectLink(ViewExecutable $view, ResultRow $row, string $linkSource) {
    if (empty($linkSource) || $linkSource == '_none') {
      return NULL;
    }

    if ($linkSource == 'default') {
      return $this->getDefaultLink($row);
    }

    $entity = $this->getEntity($row, $linkSource);
    if (!$entity->hasField($linkSource)) {
      $markup = $this->renderField($view, $row, $linkSource);
      preg_match('/<a href="([^"]+)"/', $markup, $matches);
      if (!empty($matches[1])) {
        return $matches[1];
      }
      return NULL;
    }

    return $this->getDefaultLinkFromEntity($entity, $linkSource);
  }

  /**
   * Get default canonical link for entity view.
   *
   * @param \Drupal\views\ResultRow $row
   *   The row.
   *
   * @return string|null
   *   The default absolute canonical link.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getDefaultLink(ResultRow $row) {
    $entity = $row->_entity;
    if (empty($entity)) {
      return NULL;
    }

    $entityType = $this->entityTypeManager->getDefinition($entity->getEntityTypeId());
    $path = $entityType->getLinkTemplate('canonical');
    if (!$path) {
      return NULL;
    }

    return $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
  }

  /**
   * Retrieves default link from an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $linkSource
   *   The field's name.
   *
   * @return string|null
   *   The link value.
   */
  protected function getDefaultLinkFromEntity(EntityInterface $entity, string $linkSource) {
    $link = $entity->get($linkSource)->getValue();
    $link = reset($link);
    if (!empty($link)) {
      // Convert internal links to absolute links.
      $url = Url::fromUri($link['uri']);
      if ($url->isRouted()) {
        return $url->setAbsolute()->toString();
      }
      return $link['uri'];
    }

    return NULL;
  }

  /**
   * Renders a view field.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   * @param \Drupal\views\ResultRow $row
   *   The current row.
   * @param string $fieldSource
   *   The field's id.
   *
   * @return \Drupal\Component\Render\MarkupInterface|\Drupal\views\Render\ViewsRenderPipelineMarkup|string|null
   *   The markup.
   */
  protected function renderField(ViewExecutable $view, ResultRow $row, string $fieldSource) {
    if (empty($view->field[$fieldSource]->view->row_index)) {
      $view->field[$fieldSource]->view->row_index = $row->index;
    }

    try {
      return $view->field[$fieldSource]->advancedRender($row);
    }
    catch (\LogicException) {
      $renderValue = $view->field[$fieldSource]->render($row);
      if (is_array($renderValue)) {
        return $this->renderer->renderPlain($renderValue);
      }
      return $renderValue->__toString();
    }
  }

}
