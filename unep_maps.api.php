<?php

/**
 * Alters data to be used by the UNEP MAPS module.
 *
 * @param $countryData
 *   The data to alter.
 *
 * @return void
 */
function unep_maps_unep_maps_country_data_alter(array &$countryData) {
  $countryData[] = [
    'iso3' => 'TWN',
    'popup' => 'Taiwan, Province of China',
    'link' => '/countries/chn',
  ];

  $countryData[] = [
    'iso3' => 'FLK',
    'popup' => 'Falkland Islands (Malvinas)',
    'link' => '/countries/gbr',
  ];
}
