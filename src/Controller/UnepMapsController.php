<?php

namespace Drupal\unep_maps\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Maps controller used to display the disclaimer.
 */
class UnepMapsController extends ControllerBase {

  /**
   * Displays the Carto Tile disclaimer.
   *
   * @return string[]
   */
  public function mapDisclaimer() {
    return [
      '#markup' => '<p>
              The boundaries and names shown and the designations used on this map do not imply official endorsement or
    acceptance by the United Nations. Final boundary between the Republic of Sudan and the Republic of South
              Sudan has not yet been determined.
            </p>
            <p class="disclaimer-notes">
              * Non-Self Governing Territories
    <br>** Dotted line represents approximately the Line of Control in Jammu and Kashmir agreed upon by India
    and Pakistan. The final status of Jammu and Kashmir has not yet been agreed upon by the parties.​<br>*** A
              dispute exists between the Governments of Argentina and the United Kingdom of Great Britain and Northern
              Ireland concerning sovereignty over the Falkland Islands (Malvinas).
            </p>',
    ];
  }

}
